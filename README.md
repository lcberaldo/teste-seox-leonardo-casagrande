# COMO RODAR O PROJETO EM AMBIENTE LOCAL

- Baixar e extrair uma instalação padrão do Wordpress no ambiente XAMPP, WAMPP.
- Criar um banco de dados para o site (dentro do phpmyadmin)
- Instalar o wp conectando ao banco.
- Clonar o projeto dentro da pasta 'themes'
- Executar o comando NPM i;
- Se certificar que na linha 15 do arquivo "gulpfile.js" está o caminho certo para sua instalação do wp.
- Rodar o comando Gulp

<!-- Caso o site abra, e não esteja com o tema ativo, é necessário ativá-lo por dentro do painel em "Aparencia". -->

## COMENTÁRIOS RELEVANTES

- Utilizei o gulp para minificar , concatenar arquivos e utilizar o live reload em produção.

- Deixei comentários ao longo do projeto para facilitar o entendimento de determinado trecho de código.

- Componentizei pensando em reutilizar carrossel, cards, reset, fontes e cores.

- No arquivo Style, tem os estilos da página em si, quando mais de uma página, gosto de criar um arquivo sass para cada page. Quando muito longo, gosto de separar mobile de desktop também.
