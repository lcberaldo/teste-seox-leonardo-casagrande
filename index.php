<?php get_header(); ?> <section id="videos" class="videos"><div class="head-wrapper"><div class="page-header"><h1 class="title">video</h1><div class="right-side"><a href="#" class="see-more"><span class="btn-text">Veja mais</span> <i class="fa-solid fa-angle-right"></i> </a> <?= get_template_part('carousel-controls'); ?> </div></div></div><div class="posts-wrapper"><div class="container highlight-wrapper"></div><div class="post-carousel"> <?= get_template_part('carousel'); ?> </div></div><!-- <div class="animation-wrapper">
        <div class="lds">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> --><!-- <div class="try-again">
        <button onclick="fetchData()">Tentar de novo</button>
    </div> --></section> <?php get_footer(); ?>