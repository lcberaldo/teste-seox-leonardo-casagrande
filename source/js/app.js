// função para fazer a chamada API e popular o site, exibir animação ou dropar botão de tentar de novo
const fetchData = async () => {
    const url = 'https://teste-frontend.seox.com.br/wp-json/wp/v2/posts?_embed';

    $('#videos').append(loading);

    if (tryAgain) {
        $('.try-again').remove()
    }

    try {
        const response = await fetch(url);
        const data = await response.json();

        $('.animation-wrapper').remove();

        renderGrid(data);
        splide.mount();

    } catch (error) {
        console.log('Error: ', error);

        $('#videos').append(tryAgain);

    } finally {
        $('.animation-wrapper').remove();
    }
};

const loading = ` <div class="animation-wrapper">
<div class="lds">
    <div></div>
    <div></div>
    <div></div>
</div>
</div>`;

const tryAgain = `<div class="try-again">
<button onclick="fetchData()">Tentar de novo</button>
</div>`

fetchData();