// Esse arquivo contem todas as configurações do carousel, inclusive as de estilo. 
// Não fiz na mão pra evitar ficar sem tempo, mas achei uma lib que se adequa a necessidade. 
// Tem a função Drag free no mobile, e o carousel vertical no desktop



// configurações de comportamento e estilo.
var splide = new Splide('.splide', {
    drag: 'free',
    perPage: 1,
    arrows: false,
    pagination: false,
    fixedWidth: '80%',
    gap: 16,
    padding: { left: 24, right: 24 },
    mediaQuery: 'min',
    breakpoints: {
        700: {
            gap: 24,
            fixedWidth: '70%'
        },
        1000: {
            direction: 'ttb',
            perPage: 2,
            height: 640,
            drag: false,
            fixedWidth: 327,
            gap: 4,

        },
        1440: {
            gap: 24,
            height: 655,

        }
    }
});

// função para customizar as arrows do carousel, deixando opaco conforme o index do slider.


function checkIndex() {
    let index = splide.index
    let length = splide.length

    if (index === 0) {
        $('.prev-btn').addClass('inactive');

    } else {
        $('.prev-btn').removeClass('inactive');

    }

    if (index == length - 2) {
        $('.next-btn').addClass('inactive');

    } else {
        $('.next-btn').removeClass('inactive');
    }
}

// função simples para criar os movimentos de prev e next.

$('.next-btn').on('click', function () {
    splide.go('+1')
    checkIndex()
})

$('.prev-btn').on('click', function () {
    splide.go('-1')
    checkIndex()
})