// popula o carrossel e o post destaque 

const renderCard = (post, isHighlight) => {
  const { _embedded, link, title, excerpt } = post;
  const image = _embedded['wp:featuredmedia']?.[0].source_url ?? '';

  const card = `
      <li class="splide__slide">
        <a href="${link}" target="_blank" class="item ${isHighlight ? 'highlight' : ''}">
          <div class="thumb" style="background: url(${image})"></div>
  
          <div class="post-info">
            <h2 class="hat">${title.rendered.substring(0, 30)}</h2>
            <p class="excerpt feed-3">${excerpt.rendered.substring(3, 50) + '...'}</p>
          </div>
  
          <div class="play-icon">
            <i class="fa-solid fa-play"></i>
          </div>
        </a>
      </li>
    `;

  if (isHighlight) {
    $('.highlight-wrapper').append(card);
  } else {
    $('.splide__list').append(card);
  }
};

const renderGrid = (data) => {
  data.forEach((post, index) => {
    const isTheFirstIndex = index === 0;
    renderCard(post, isTheFirstIndex);
  });
};